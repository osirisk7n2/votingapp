﻿using System.Web;
using System.Web.Optimization;

namespace VotingApp
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/css")
                .Include("~/Content/bootstrap.css")
                .Include("~/Content/angular-toastr.css")
                .Include("~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/VotingApp")
                .Include("~/Scripts/jquery-2.2.0.js")
                .Include("~/Scripts/moment.js")
                .Include("~/Scripts/bootstrap.js")
                .Include("~/Scripts/angular.js")
                .Include("~/Scripts/angular-toastr.js")
                .Include("~/Scripts/angular-strap.js")
                .Include("~/Scripts/angular-strap.tpl.js")
                .Include("~/Scripts/angular-route.js")
                .Include("~/Scripts/Custom/app.js")
                .Include("~/Scripts/Angular/Filters/MomentFilter.js")
                .Include("~/Scripts/Angular/VotingAppModule.js")
                .Include("~/Scripts/Angular/VotingAppRouteConfiguration.js")
                .IncludeDirectory("~/Scripts/Angular/Services", "*.js")
                .IncludeDirectory("~/Scripts/Angular/Controllers", "*.js"));


            BundleTable.EnableOptimizations = false;
        }
    }
}
