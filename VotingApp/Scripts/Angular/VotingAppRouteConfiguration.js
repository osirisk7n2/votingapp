﻿(function() {
    var app = angular.module('VotingApp');

    app.config([
    '$routeProvider', function ($routeProvider) {
        $routeProvider
            .when('/Index', {
                templateUrl: '/Home/IndexPartTemplate',
            })
            .when('/ExistingSurveys', {
                templateUrl: '/Surveys/ExistingSurveysTemplate',
            })
            .when('/Create', {
                templateUrl: '/Surveys/CreateSurveysMainTemplate',
            })
            .when('/ShowSurvey/:id', {
                templateUrl: 'Surveys/ShowSurveyTemplate',
            })
            .otherwise({
                redirectTo: '/Index'
            });
    }
    ]);
})();