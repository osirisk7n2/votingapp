﻿(function () {
    var app = angular.module('VotingApp');

    app.factory('SurveyService', [
        '$http', '$q', function ($http, $q) {

            return {
                GetSurvey: function (id) {
                    return $http.get('/Surveys/GetSurvey/' + id);
                },
                GetSurveyList: function () {
                    return $http.get('/Surveys/GetSurveyList');
                },
                SubmitSurvey: function (data) {
                    return $http.post('/Surveys/SubmitSurvey', data);
                },
                RemoveSurvey: function(id) {
                    return $http.post('/Surveys/RemoveSurvey/' + id);
                }
            };
        }
    ]);
})();
