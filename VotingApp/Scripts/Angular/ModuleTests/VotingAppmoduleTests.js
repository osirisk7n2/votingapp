﻿describe('Controller : VotingAppController',function() {
    beforeEach(module('VotingApp'));

    var ctrl;
    beforeEach(inject(function($controller) {
        ctrl = $controller('BrowseSurveysController');
    }));

    it('powinna byc jakas lista', function() {
        expect(ctrl.surveys).toBeTruthy();
    });

    it('powinny byc elementy w liscie', function() {
        expect(ctrl.surveys.length).toBeGreaterThan(0);
    });
})