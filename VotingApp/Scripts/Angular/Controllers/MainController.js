﻿(function () {
    var application = angular.module('VotingApp');

    application.controller('MainController', ['$filter', 'SurveyService', 'toastr',
        function ($filter, surveyService, toastr) {
            var self = this;
            self.survey = {
                Questions: []
            };

            self.QuestionId = 1;
            self.QuestionOptionId = 1;

            self.AddQuestion = function () {
                self.survey.Questions.push({
                    QuestionKind: '',
                    QuestionText: "",
                    Id: self.QuestionId,
                    QuestionOption: []
                });
                self.QuestionId++;
            };

            self.AddQuestionOption = function (questionId) {
                var question = $filter('filter')(self.survey.Questions, function (e) {
                    return e.Id === questionId;
                })[0];
                question.QuestionOption.push({
                    Id: self.QuestionOptionId,
                    Text: ""
                });
                self.QuestionOptionId++;
            }

            self.Submit = function () {
                surveyService.SubmitSurvey(self.survey).then(function (response) {
                    if (response.data.Result === "Succes") {
                        toastr.success('Zapisano ankiete!');

                        self.survey = {
                            Questions: []
                        };
                    } else {
                        toastr.error('Błąd przy zapisie ankiety..');
                    }
                });
            }
        }
    ]);
})();