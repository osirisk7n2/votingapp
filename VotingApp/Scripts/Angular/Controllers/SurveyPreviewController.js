﻿(function() {
    var app = angular.module('VotingApp');

    app.controller('SurveyPreviewController', [
        '$routeParams', 'SurveyService', function($routeParams, surveyService) {
            var self = this;
            self.surveyId = $routeParams.id;
            surveyService.GetSurvey(self.surveyId).then(function(data) {
                self.survey = data.data;
            });
        }
    ]);
})();