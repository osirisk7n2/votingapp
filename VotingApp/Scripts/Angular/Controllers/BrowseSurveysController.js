﻿(function () {
    var app = angular.module('VotingApp');

    app.controller('BrowseSurveysController', ['SurveyService', 'toastr',
        function (surveyService, toastr) {
            var self = this;
            self.surveys = [];

            surveyService.GetSurveyList().then(function (ret) {
                self.surveys = ret.data;
            });

            self.RemoveSurvey = function (id) {
                surveyService.RemoveSurvey(id).then(function() {
                    toastr.success('Poprawnie usunieto ankiete');

                    surveyService.GetSurveyList().then(function (ret) {
                        self.surveys = ret.data;
                    });
                });
                console.log("remove clicked");
            };
        }
    ]);
})();