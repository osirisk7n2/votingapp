﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using VotingApp.Enum;
using VotingApp.Models.Survey;
using VotingApp.Models.SurveyDB;
using VotingApp.Services.Interface.Surveys;

namespace VotingApp.Services.Implementation.Surveys
{
    public class SurveyOperationsService : ISurveyOperationsService
    {
        public IEnumerable<SurveyViewModel> GetSurveyList()
        {
            IList<SurveyViewModel> results;

            using (var context = new SurveyContext())
            {
                var surveys = context.Survey.Where(x=>x.IsActiveYN).ToList();

                results = Mapper.Map<IList<Survey>, SurveyViewModel[]>(surveys);
            }

            return results;
        }

        public SurveyViewModel GetSurvey(int id)
        {
            using (var context = new SurveyContext())
            {
                var item = context.Survey.FirstOrDefault(x => x.Id == id);

                return Mapper.Map<Survey, SurveyViewModel>(item);
            }
        }

        public void SaveSurvey(SurveyViewModel model)
        {
            var item = Mapper.Map<SurveyViewModel, Survey>(model);

            using (var surveyContext = new SurveyContext())
            {
                surveyContext.Survey.Add(item);
                surveyContext.SaveChanges();
            }
        }

        public void RemoveSurvey(int id)
        {
            using (var context = new SurveyContext())
            {
                var item = context.Survey.First(x => x.Id == id);
                item.IsActiveYN = false;
                context.SaveChanges();
            }
        }
    }
}