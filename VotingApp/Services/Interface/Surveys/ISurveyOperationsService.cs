﻿using System.Collections;
using System.Collections.Generic;
using VotingApp.Models.Survey;
using VotingApp.Plumbing.Markers;

namespace VotingApp.Services.Interface.Surveys
{
    public interface ISurveyOperationsService : ITransistentDependency
    {
        IEnumerable<SurveyViewModel> GetSurveyList();
        SurveyViewModel GetSurvey(int id);
        void SaveSurvey(SurveyViewModel model);
        void RemoveSurvey(int id);
    }
}