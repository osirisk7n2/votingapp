﻿namespace VotingApp.Enum
{
    public enum SurveyQuestionKindEnum
    {
        Open = 1,
        CloseOneAnswer = 2,
        CloseMultipleAnswers = 3
    }
}