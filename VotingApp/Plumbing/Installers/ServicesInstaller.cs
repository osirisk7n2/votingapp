﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using VotingApp.Plumbing.Markers;

namespace VotingApp.Plumbing.Installers
{
    public class ServicesInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromThisAssembly()
                .BasedOn<ITransistentDependency>()
                .WithServiceDefaultInterfaces()
                .LifestyleTransient());
        }
    }
}