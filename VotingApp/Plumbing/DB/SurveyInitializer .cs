﻿using System.Data.Entity;
using VotingApp.Models.SurveyDB;

namespace VotingApp.Plumbing.DB
{
    public class SurveyInitializer : DropCreateDatabaseIfModelChanges<SurveyContext>
    {
        protected override void Seed(SurveyContext context)
        {
            base.Seed(context);
        }
    }
}