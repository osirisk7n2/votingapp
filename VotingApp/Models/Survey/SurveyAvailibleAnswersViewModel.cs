﻿namespace VotingApp.Models.Survey
{
    public class SurveyAvailibleAnswersViewModel
    {
        public int Id { get; set; }
        public string Text { get; set; }
    }
}