﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace VotingApp.Models.Survey
{
    public class SurveyViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime CreateDate { get; set; }
        public IList<SurveyQuestionViewModel> Questions { get; set; }
    }
}