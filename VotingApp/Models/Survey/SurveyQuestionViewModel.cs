﻿using System.Collections;
using System.Collections.Generic;
using VotingApp.Enum;

namespace VotingApp.Models.Survey
{
    public class SurveyQuestionViewModel
    {
        public int Id { get; set; }
        public string QuestionText { get; set; }

        public SurveyQuestionKindEnum QuestionKind { get; set; }

        public IList<SurveyAvailibleAnswersViewModel> QuestionOption { get; set; }
    }
}