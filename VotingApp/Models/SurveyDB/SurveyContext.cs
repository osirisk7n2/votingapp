﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using VotingApp.Models.SurveyDB;

namespace VotingApp.Models.SurveyDB
{
    public class SurveyContext : DbContext
    {
        public SurveyContext() : base("SurveyContext")
        {
            Database.SetInitializer<SurveyContext>(new System.Data.Entity.DropCreateDatabaseIfModelChanges<SurveyContext>());
        }

        public DbSet<Survey> Survey { get; set; }
        public DbSet<SurveyXQuestion> SurveyXQuestion { get; set; }
        public DbSet<Question> Question { get; set; }
        public DbSet<QuestionType> QuestionType { get; set; }
        public DbSet<QuestionXAnswer> QuestionXAnswer { get; set; }
        public DbSet<QuestionXGivenAnswer> QuestionXGivenAnswer { get; set; }
        public DbSet<Answer> Answer { get; set; }
        public DbSet<GivenAnswer> GivenAnswer { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}