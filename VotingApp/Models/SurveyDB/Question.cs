﻿using System;
using System.Collections.Generic;
using Castle.Core.Internal;
using VotingApp.Models.SurveyDB.Enums;

namespace VotingApp.Models.SurveyDB
{
    public class Question
    {
        public int Id { get; set; }
        public DateTime CreateDate { get; set; }
        public bool IsActiveYN { get; set; }
        public virtual QuestionTypeEnum QuestionType { get; set; }
        public string QuestionContent { get; set; }

        public virtual ICollection<SurveyXQuestion> SurveyXQuestions { get; set; }
        public virtual ICollection<QuestionXAnswer> QuestionXAnswers { get; set; }
        public virtual ICollection<QuestionXGivenAnswer> QuestionXGivenAnswers { get; set; }

        public void SetQuestionParent()
        {
            QuestionXAnswers.ForEach(x => x.Question = this);
        }
    }
}