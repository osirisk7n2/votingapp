﻿namespace VotingApp.Models.SurveyDB
{
    public class SurveyXQuestion
    {
        public int Id { get; set; }
        public bool IsActiveYN {get;set;}

        public virtual Survey Survey { get; set; }
        public virtual Question Question { get; set; }
    }
}