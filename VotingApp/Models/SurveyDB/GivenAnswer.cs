﻿using System;
using System.Collections.Generic;

namespace VotingApp.Models.SurveyDB
{
    public class GivenAnswer
    {
        public int Id { get; set; }
        public DateTime CreateDate { get; set; }
        public bool IsActiveYN { get; set; }
        public string GivenAnswerContent { get; set; }

        public virtual ICollection<QuestionXGivenAnswer> QuestionXGivenAnswers { get; set; }
    }
}