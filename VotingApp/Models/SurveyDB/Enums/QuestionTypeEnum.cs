﻿namespace VotingApp.Models.SurveyDB.Enums
{
    public enum QuestionTypeEnum
    {
        Open = 1,
        CloseOneAnswer = 2,
        CloseMultipleAnswers = 3
    }
}