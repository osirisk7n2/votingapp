﻿namespace VotingApp.Models.SurveyDB
{
    public class QuestionType
    {
        public int Id { get; set; }
        public string TypeName { get; set; }
        public virtual Question Question { get; set; }
    }
}