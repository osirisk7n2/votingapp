﻿using System;
using System.Collections.Generic;
using Castle.Core.Internal;

namespace VotingApp.Models.SurveyDB
{
    public class Survey
    {
        public int Id { get; set; }
        public DateTime CreateDate { get; set; }
        public bool IsActiveYN { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public virtual ICollection<SurveyXQuestion> SurveyXQuestions { get; set; }

        public void SetSurveyAsParent()
        {
            SurveyXQuestions.ForEach(x => x.Survey = this);
        }
    }
}