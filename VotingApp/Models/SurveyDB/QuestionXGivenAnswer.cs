﻿namespace VotingApp.Models.SurveyDB
{
    public class QuestionXGivenAnswer
    {
        public int Id { get; set; }
        public bool IsActiveYN { get; set; }

        public virtual Question Question { get; set; }
        public virtual GivenAnswer Answer { get; set; }
    }
}