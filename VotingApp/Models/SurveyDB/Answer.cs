﻿using System;
using System.Collections.Generic;

namespace VotingApp.Models.SurveyDB
{
    public class Answer
    {
        public int Id { get; set; }
        public DateTime CreateDate { get; set; }
        public bool IsActiveYN { get; set; }
        public string AnswerContent { get; set; }

        public virtual ICollection<QuestionXAnswer> QuestionXAnswers { get; set; }
    }
}