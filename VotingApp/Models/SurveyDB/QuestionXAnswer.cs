﻿namespace VotingApp.Models.SurveyDB
{
    public class QuestionXAnswer
    {
        public int Id { get; set; }
        public bool IsActiveYN { get; set; }

        public virtual Question Question { get; set; }
        public virtual Answer Answer { get; set; }
    }
}