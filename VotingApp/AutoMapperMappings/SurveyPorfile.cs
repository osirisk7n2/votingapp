﻿using System;
using System.Web.Optimization;
using System.Web.UI;
using AutoMapper;
using Castle.Core.Internal;
using VotingApp.Enum;
using VotingApp.Models.Survey;
using VotingApp.Models.SurveyDB;
using VotingApp.Models.SurveyDB.Enums;

namespace VotingApp.AutoMapperMappings
{
    public class SurveyPorfile : Profile
    {
        protected override void Configure()
        {
            SurveyInternalToExternlMapCreate();
            SurveyExternalToInternalMapCreate();
        }

        private static void SurveyInternalToExternlMapCreate()
        {
            Mapper.CreateMap<SurveyViewModel, Survey>()
                .ForMember(x => x.CreateDate, opts => opts.UseValue(DateTime.Now))
                .ForMember(x => x.IsActiveYN, opts => opts.UseValue(true))
                .ForMember(x => x.Description, opts => opts.MapFrom(src => src.Description))
                .ForMember(x => x.Title, opts => opts.MapFrom(src => src.Title))
                .ForMember(x => x.SurveyXQuestions, opts => opts.MapFrom(src => src.Questions))
                .ForMember(x => x.Id, opts => opts.Ignore())
                .AfterMap((src, dest) => dest.SetSurveyAsParent());

            Mapper.CreateMap<SurveyQuestionViewModel, SurveyXQuestion>()
                .ForMember(x => x.IsActiveYN, opts => opts.UseValue(true))
                .ForMember(x => x.Question, opts => opts.MapFrom(src => src))
                .ForMember(x => x.Id, opts => opts.Ignore());


            Mapper.CreateMap<SurveyQuestionViewModel, Question>()
                .ForMember(x => x.QuestionType, opts => opts.MapFrom(src => (QuestionTypeEnum)((int)src.QuestionKind)))
                .ForMember(x => x.CreateDate, opts => opts.UseValue(DateTime.Now))
                .ForMember(x => x.IsActiveYN, opts => opts.UseValue(true))
                .ForMember(x => x.QuestionContent, opts => opts.MapFrom(src => src.QuestionText))
                .ForMember(x => x.QuestionXAnswers, opts => opts.MapFrom(src => src.QuestionOption))
                .ForMember(x => x.Id, opts => opts.Ignore())
                .AfterMap((src, dest) => dest.SetQuestionParent());

            Mapper.CreateMap<SurveyAvailibleAnswersViewModel, QuestionXAnswer>()
                .ForMember(x => x.Id, opts => opts.Ignore())
                .ForMember(x => x.IsActiveYN, opts => opts.UseValue(true))
                .ForMember(x => x.Answer, opts => opts.MapFrom(src => src));

            Mapper.CreateMap<SurveyAvailibleAnswersViewModel, Answer>()
                .ForMember(x => x.Id, opts => opts.Ignore())
                .ForMember(x => x.IsActiveYN, opts => opts.UseValue(true))
                .ForMember(x => x.CreateDate, opts => opts.UseValue(DateTime.Now))
                .ForMember(x => x.AnswerContent, opts => opts.MapFrom(src => src.Text));
        }

        private static void SurveyExternalToInternalMapCreate()
        {
            Mapper.CreateMap<Survey, SurveyViewModel>()
                .ForMember(x => x.Description, opts => opts.MapFrom(src => src.Description))
                .ForMember(x => x.Title, opts => opts.MapFrom(src => src.Title))
                .ForMember(x => x.CreateDate, opts => opts.MapFrom(src => src.CreateDate))
                .ForMember(x => x.Questions, opts => opts.MapFrom(src => src.SurveyXQuestions));

            Mapper.CreateMap<SurveyXQuestion, SurveyQuestionViewModel>()
                .ForMember(x => x.QuestionKind,
                    opts => opts.MapFrom(src => (SurveyQuestionKindEnum)((int)src.Question.QuestionType)))
                .ForMember(x => x.QuestionText, opts => opts.MapFrom(src => src.Question.QuestionContent))
                .ForMember(x => x.Id, opts => opts.MapFrom(src => src.Question.Id))
                .ForMember(x => x.QuestionOption, opts => opts.MapFrom(src => src.Question.QuestionXAnswers));

            Mapper.CreateMap<QuestionXAnswer, SurveyAvailibleAnswersViewModel>()
                .ForMember(x => x.Id, opts => opts.MapFrom(src => src.Answer.Id))
                .ForMember(x => x.Text, opts => opts.MapFrom(src => src.Answer.AnswerContent));
        }
    }
}