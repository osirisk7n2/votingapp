﻿using System;
using System.Web.Mvc;
using VotingApp.Models.Survey;
using VotingApp.Services.Interface.Surveys;

namespace VotingApp.Controllers
{
    public class SurveysController : Controller
    {
        private readonly ISurveyOperationsService _surveyOperationsService;

        public SurveysController(ISurveyOperationsService surveyOperationsService)
        {
            _surveyOperationsService = surveyOperationsService;
        }

        // GET: Surveys
        public PartialViewResult ExistingSurveysTemplate()
        {
            return PartialView("Partial/_ExistingSurveysTemplate");
        }

        public PartialViewResult CreateSurveysMainTemplate()
        {
            return PartialView("Partial/_CreateSurveysMainTemplate");
        }

        public PartialViewResult ShowSurveyTemplate()
        {
            return PartialView("Partial/_ShowSurveyTemplate");
        }

        public JsonResult GetSurvey(int id)
        {
            var survey = _surveyOperationsService.GetSurvey(id);

            return new JsonResult { Data = survey, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult GetSurveyList()
        {
            var surveyList = _surveyOperationsService.GetSurveyList();

            return new JsonResult { Data = surveyList, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        public JsonResult SubmitSurvey(SurveyViewModel model)
        {
            try
            {
                _surveyOperationsService.SaveSurvey(model);
                return new JsonResult { Data = new { Result = "Succes" } };
            }
            catch (Exception ex)
            {
                return new JsonResult
                {
                    Data = new { Result = "Fail" }
                };
            }
        }

        [HttpPost]
        public JsonResult RemoveSurvey(int id)
        {
            try
            {
                _surveyOperationsService.RemoveSurvey(id);
                return new JsonResult { Data = new { Results = "Success" } };
            }
            catch (Exception ex)
            {
                return new JsonResult
                {
                    Data = new { Results = "Fail" }
                };
            }
        }
    }
}